<?php
session_start();
require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/OrderController.php";
if (isset($_POST["button"]) && $_POST["button"] === "purchase") {
    $arr = array();
    $arr["itemId"] = $_POST["id"];
    $arr["count"] = $_POST["count"];
    $arr["userId"] = $_SESSION["id"];
    createOrder($arr);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Работилничка</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta name="theme-color" content="#507742">
    <link rel="icon" sizes="124x124" href="favicon.ico">
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>

</head>
<body>
<img class="logo" alt="logo" src="img/logo.png" usemap="#bgmap">
<br/>
<div class="topnav" id="myTopnav">
    <a href="index.html"> <img src="img/home.png" height="15" width="15"> </a>
    <a href="artic.php" class="active"> Артикули</a>
    <a href="about.html">За нас</a>
    <a href="more_info.html">Какво е онлайн магазин?</a>
    <a href="login.php" class="right"><img src="img/login.png"></a>
    <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>

</div>
<br/>
<br/>
<?php
function stripQuotes($text)
{
    return preg_replace('/^(\'(.*)\'|"(.*)")$/', '$2$3', $text);
}

function buttons($count)
{

    if (isset($_SESSION['username']) && $_SESSION['username'] != "admin") {
        $button = "  Брой:<form action='item.php?id=" . $_GET["id"] . "' method='post' ><input type='hidden' name='id' value=" . $_GET["id"] . "><select name='count'>";
        for ($i = 0; $i < 10 && $i <= $count; $i++) {
            $button .= "<option value='$i'>$i</option>";
        }
        $button .= "</select><button type=\"submit\" class=\"button\" name='button' value='purchase'>Поръчай</button></form>";
    } elseif (isset($_SESSION['username']) && $_SESSION['username'] === "admin") {
        $button = "<span style='float:left;'><form action =\"admin/edit.php\" method='post'><input type='hidden' name='id' value=" . $_GET["id"] . "><button type=\"submit\" name='submit' value='update' class=\"buttonImport\">Edit</button></form></span>
                   <span style='float:right;'><form action =\"admin/delete.php\" method='post'><input type='hidden' name='id' value=" . $_GET["id"] . "><button type=\"submit\" name='submit' value='delete' class=\"buttonError\">Delete</button></form></span>";
    } else {
        $button = "Моля влезте в профила си";
    }
    return $button;
}

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    require_once "../Controller/ItemController.php";
    $result = readOne($id);
    if (!isset($result["name"])) {
        header("Location: artic.php");
    }
    echo " <div class=\"pic\"><a href=\"img/" . $result["picture"] . "\"><img class=\"pic\" src=\"img/" . $result["picture"] . "\"></a></div>
        <div class=\"Info\">
            <table class=\"itemInfo\">
                <tr><td>Име:</td> <td>" . $result["name"] . "</td></tr>
             <tr><td>Тегло:</td> <td>" . $result["weight"] . "</td></tr>
            <tr><td> Размери:</td><td>" . $result["size"] . "</td></tr>
             <tr><td>Налично количество:</td><td> " . $result["count"] . " </td></tr>
             <tr><td>Каталожен №:</td><td> " . $id . " </td></tr>
            <tr><td> Цена:</td><td>" . $result["price"] . " лв.</td></tr>
             <tr><td colspan='2'\> " . buttons($result["count"]) . "</td></tr>
            </table>
        </div>
        <div class=\"moreInfo\"> " . stripQuotes($result["info"]) . "</div>";
} else {
    header("Location: artic.php");
}


?>

</body>
</html>
