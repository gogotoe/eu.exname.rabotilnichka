<!DOCTYPE html>
<html>
<head>
    <title>Работилничка</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta name="theme-color" content="#507742">
    <link rel="icon" sizes="124x124" href="favicon.ico">
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>

</head>
<body>
<img class="logo" alt="logo" src="img/logo.png" usemap="#bgmap">
<div class="topnav" id="myTopnav">
    <a href="index.html"> <img src="img/home.png" height="15" width="15"> </a>
    <a href="artic.php" class="active"> Артикули</a>
    <a href="about.html">За нас</a>
    <a href="more_info.html">Какво е онлайн магазин?</a>
    <a href="login.php" class="right"><img src="img/login.png"></a>
    <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
<br/>
<br/>
<div class="itemsdiv">
    <?php
    require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/ItemController.php";
    $itemsArray = readAll();
    foreach ($itemsArray as $item) {
        echo "<div class=\"items\">
                <a href=\"img/" . $item["picture"] . "\" target=\"_blank\"><img src=img/" . $item["thumbnail"] . " class=\"tumbnail\" ></a>
                <a style=\"font-size: 10px;\">" . $item["id"] . "</a>
                <br>
                <a href=\"item.php?id=" . $item["id"] . "\" name=\"1\">" . $item["name"] . "</a>
                <br/>
                <p align=\"center\">" . $item["price"] . " лв.</p>
            </div>";
    }
    ?>
</div>
<script>
    var cntv;
    var reqn;

    function cnt(n) {
        var d = document.getElementById("c").value;
        cntv = d;
        reqn = n;
    }

    function linkin(n) {
        if (n == reqn) {
            return window.location.href = '#?id=' + n + '&cnt=' + cntv;
        } else {
            alert("Изберете количество преди да добавите предмета към количката си");
            return window.location.href = '#';
        }
    }
</script>
</body>
</html>
