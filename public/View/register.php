<?php

if (isset($_POST["submit"]) && $_POST["submit"] == "register") {
    require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/UserController.php";
    $req = Array();
    $req["username"] = $_POST["username"];
    $req["firstName"] = $_POST["firstName"];
    $req["lastName"] = $_POST["lastName"];
    $req["password"] = $_POST["password"];
    $req["email"] = $_POST["email"];
    $req["address"] = $_POST["address"];
    create($req);
}
?>
<html>
<head>
    <title>Работилничка</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta name="theme-color" content="#507742">
    <link rel="icon" sizes="124x124" href="favicon.ico">
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>

</head>
<body>
<img class="logo" alt="logo" src="img/logo.png" usemap="#bgmap">
<map name="bgmap">
    <area shape="circle" coords="136,53,25" href="videos.html"/>
</map>
<br/>
<div class="topnav" id="myTopnav">
    <a href="index.html"> <img src="img/home.png" height="15" width="15"> </a>
    <a href="artic.php"> Артикули</a>
    <a href="about.html">За нас</a>
    <a href="more_info.html">Какво е онлайн магазин?</a>
    <a href="login.php" class="right"><img src="img/login.png"></a>
    <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
<br/>

<div class="cen">
    <h2 align="center">Регистрация</h2>
    <form action="register.php" method="post">
        <div style="margin-left : 29%" class="query">
            <table>

                <tr>
                    <td> Потребителско име:</td>
                    <td><input type="text" name="username"></td>
                </tr>

                <tr>
                    <td> Имейл:</td>
                    <td><input type="email" name="email"></td>
                </tr>
                <tr>
                    <td>
                        Парола:
                    </td>
                    <td><input type="password" name="password">
                    </td>
                </tr>
                <tr>
                    <td>
                        Повторете паролата:
                    </td>
                    <td><input type="password" name="secPassword">
                    </td>
                </tr>
                <tr>
                    <td>
                        Име:
                    </td>
                    <td><input type="text" name="firstName">
                    </td>
                </tr>
                <tr>
                    <td>
                        Фамилия:
                    </td>
                    <td><input type="text" name="lastName">
                    </td>
                </tr>
                <tr>
                    <td>
                        Адрес:
                    </td>
                    <td><input type="text" name="address">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p align="center">
                            <button type="submit" name="submit" value="register" class="buttonOk">Продължи</button>
                        </p>
                    </td>
                </tr>

            </table>
        </div>
    </form>
    <p align="center" style="margin-left: 5%;">
        <button type="register" onclick="onLogin()" class="buttonImport">Вход</button>
    </p>
</div>
</body>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }

    function onLogin() {
        window.location.href = "login.php";
    }
</script>
</html>
