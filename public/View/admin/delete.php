<?php
/**
 * User: exname
 * Date: 2/10/19
 * Time: 11:05 AM
 */

session_start();
if (!isset($_SESSION['username']) || $_SESSION["username"] != "admin") {
    echo "<script>setTimeout('top.location = \'index.php\'', 0);</script>";
} elseif (isset($_POST["submit"]) && $_POST["submit"] == "delete" && $_POST["id"]) {
    require_once "../../Controller/ItemController.php";
    echo "<script>alert(\"Are you sure ?\")</script>";
    delete($_POST["id"]);
    echo "<script>setTimeout('top.location = \'../artic.php\'', 0);</script>";
} else {
    ?>

    <html>
    <head>
        <title>Работилничка</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <meta name="theme-color" content="#507742">
        <link rel="icon" sizes="124x124" href="../favicon.ico">
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    </head>
    <body>
    <img class="logo" alt="logo" src="../img/logo.png" usemap="#bgmap">
    <br/>
    <div class="topnav" id="myTopnav">
        <a href="index.php" class="active"> <img src="../img/home.png" height="15" width="15"> </a>
        <a href="item.php" class="active">Нов артикул</a>
        <a href="purchase.php">Заявени покупки</a>
        <a href="../index.html">Назад към онлайн магазина</a>
        <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>
    </div>
    <div class="cen">
                   
        <h3 align="center"><p>Ид-то е невалидно</p></h3>
    </div>
    </body>
               
    <script>setTimeout('top.location = \'../artic.php\'', 1000);</script>

    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
    </html>

    <?php
}
?>