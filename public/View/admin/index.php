<!DOCTYPE html>
<?php session_start();
if (isset($_GET['logout'])) {
    $_SESSION['username'] = '';
    $_SESSION['id'] = 0;
    session_destroy();
    echo "<script>setTimeout('top.location = \'index.php\'', 0);</script>";
}
if (!isset($_SESSION["username"]) || $_SESSION["username"] != "admin"){
    echo "<script>setTimeout('top.location = \'../index.html\'', 0);</script>";
}else{
?>
<html>
<head>
    <title>Работилничка</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <meta name="theme-color" content="#507742">
    <link rel="icon" sizes="124x124" href="../favicon.ico">


    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>

</head>
<body>
<img class="logo" alt="logo" src="../img/logo.png" usemap="#bgmap">
<map name="bgmap">
    <area shape="circle" coords="136,53,25" href="videos.html"/>
</map>
<div class="topnav" id="myTopnav">
    <a href="index.php" class="active"> <img src="../img/home.png" height="15" width="15"> </a>
    <a href="item.php">Нов артикул</a>
    <a href="purchase.php">Заявени покупки</a>
    <a href="../index.html">Назад към онлайн магазина</a>
    <?php if (isset($_SESSION['username'])) {
        if ($_SESSION['username']): ?>
            <a href="index.php?logout=1" class="right">Изход</a>
        <?php endif;
    } ?>
    <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>


</div>
<br/>
</body>
</html>
<?php } ?>