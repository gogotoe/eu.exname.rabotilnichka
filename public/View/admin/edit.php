<?php
/**
 * User: exname
 * Date: 2/10/19
 * Time: 11:05 AM
 */

session_start();

if (!isset($_SESSION["username"]) || $_SESSION["username"] != "admin") {
    echo "<script>setTimeout('top.location = \'../index.html\'', 0);</script>";
} elseif (isset($_POST["submit"]) && $_POST["submit"] == "edit") {
    $request["name"] = $_POST["name"];
    $request["weight"] = $_POST["weight"];
    $request["size"] = $_POST["size"];
    $request["count"] = $_POST["count"];
    $request["price"] = $_POST["price"];
    $request["category"] = $_POST["category"];
    $request["info"] = $_POST["info"];
    require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/ItemController.php";
    update($_POST["id"], $request);
    header("Location: index.php");
} elseif (isset($_POST["id"])) {
    $id = $_POST["id"];
    require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/ItemController.php";
    $result = readOne($id);
    if (!isset($result["name"])) {
        header("Location: index.php");
    }
    ?>
    <html>
    <head>
        <title>Работилничка</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <meta name="theme-color" content="#507742">
        <link rel="icon" sizes="124x124" href="../favicon.ico">
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    </head>
    <body>
    <img class="logo" alt="logo" src="../img/logo.png" usemap="#bgmap">
    <br/>
    <div class="topnav" id="myTopnav">
        <a href="index.php" class="active"> <img src="../img/home.png" height="15" width="15"> </a>
        <a href="item.php" class="active">Нов артикул</a>
        <a href="purchase.php">Заявени покупки</a>
        <a href="../index.html">Назад към онлайн магазина</a>
        <?php if (isset($_SESSION['username'])) {
            if ($_SESSION['username']): ?>
                <a href="index.php?logout=1" class="right">Изход</a>
            <?php endif;
        } ?>
        <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>
    </div>
    <br/>
    <div class="cen">
        <h2 align="center">Предмет</h2>
        <div class="centerA">
            <table>
                <form action="edit.php?submit=edit" method="post">
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <tr>
                        <td>Име</td>
                        <td><input name=" name" value="<?php if (isset($result["name"])) {
                                echo $result["name"];
                            } ?>" type="text"></td>
                    </tr>
                    <br>
                    <tr>
                        <td>Тегло</td>
                        <td><input name="weight" value="<?php if (isset($result["weight"])) {
                                echo $result["weight"];
                            } ?>" type="text"></td>
                    </tr>
                    <br>
                    <tr>
                        <td>Размери</td>
                        <td><input name="size" value="<?php if (isset($result["size"])) {
                                echo $result["size"];
                            } ?>" type="text"></td>
                    </tr>
                    <br>
                    <tr>
                        <td>Налични бройки</td>
                        <td><input name="count" value="<?php if (isset($result["count"])) {
                                echo $result["count"];
                            } ?>" type="number"></td>
                    </tr>
                    <br>
                    <tr>
                        <td>Цена</td>
                        <td><input name="price" value="<?php if (isset($result["price"])) {
                                echo $result["price"];
                            } ?>" type="number"></td>
                    </tr>
                    <br>
                    <tr>
                        <td>Категория</td>
                        <td><input name="category" value="<?php if (isset($result["category"])) {
                                echo $result["category"];
                            } ?>" type="text"></td>
                    </tr>
                    <br>
                    <tr>
                        <td>Информация</td>
                    </tr>
                    <br>
            </table>
        </div>
        <input name="info" type="hidden">
        <div style="background-color: #EEEEEE; height: 30%;" id="editor">
            <?php if (isset($result["info"])) {
                $text = preg_replace('/^(\'(.*)\'|"(.*)")$/', '$2$3', $result["info"]);
                echo $text;
            } ?>
        </div>

        <br>

        <button type="submit" name="submit" value="edit" class="button">Submit</button>
        </form>


    </div>
    </body>
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>
        var quill = new Quill('#editor', {
            theme: 'snow'
        });
        var form = document.querySelector('form');
        form.onsubmit = function () {
            var about = document.querySelector('input[name=info]');
            about.value = JSON.stringify(quill.root.innerHTML);
            return true;
        };
    </script>

    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
    </html>
    <?php
} else {
    header("Location: index.php");
}
?>
