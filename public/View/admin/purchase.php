<!DOCTYPE html>
<?php session_start();
if (isset($_GET['logout'])) {
    $_SESSION['username'] = '';
    $_SESSION['id'] = 0;
    session_destroy();
    echo "<script>setTimeout('top.location = \'index.php\'', 0);</script>";
}
if (!isset($_SESSION["username"]) || $_SESSION["username"] != "admin"){
    echo "<script>setTimeout('top.location = \'../index.html\'', 0);</script>";
}else{
include_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/OrderController.php";
if (isset($_GET["action"]) && $_GET["action"] === "delete") {
    deleteOrder($_GET["id"]);
}

?>
<html>
<head>
    <title>Работилничка</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <meta name="theme-color" content="#507742">
    <link rel="icon" sizes="124x124" href="../favicon.ico">


    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>

</head>
<body>
<img class="logo" alt="logo" src="../img/logo.png" usemap="#bgmap">
<map name="bgmap">
    <area shape="circle" coords="136,53,25" href="videos.html"/>
</map>
<div class="topnav" id="myTopnav">
    <a href="index.php"> <img src="../img/home.png" height="15" width="15"> </a>
    <a href="item.php">Нов артикул</a>
    <a href="purchase.php" class="active">Заявени покупки</a>
    <a href="../index.html">Назад към онлайн магазина</a>
    <?php if (isset($_SESSION['username'])) {
        if ($_SESSION['username']): ?>
            <a href="index.php?logout=1" class="right">Изход</a>
        <?php endif;
    } ?>
    <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
<table class="itemInfo">
    <thead>
    <tr>
        <th>Кат. №</th>
        <th>Име</th>
        <th>Цена</th>
        <th>Брой</th>
        <th>Купувач</th>
        <th>Адрес за доставка</th>
        <th>Email</th>
        <th>Обща сума</th>
        <th>Действие</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $results = readAllOrders();
    foreach ($results as $result)
        echo "<tr>
        <td>" . $result["id"] . "</td>
        <td>" . $result["name"] . "</td>
        <td>" . $result["price"] . " лв.</td>
        <td> " . $result["count"] . "</td>
        <td> " . $result["userNames"] . "</td>
        <td>" . $result["address"] . "</td>
        <td>" . $result["email"] . "</td>   
        <td>" . $result["total"] . " лв.</td>
        <td><a href='purchase.php?id=" . $result["oId"] . "&action=delete' ><img src='../img/delete.png' </td>   
    </tr>";
    ?>
    </tbody>
</table>
<br/>
</body>
</html>
<?php } ?>