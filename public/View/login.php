<?php
session_start();
if (isset($_POST["submit"]) && $_POST["submit"] == "login" && !isset($_SESSION["username"])) {
    require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/UserController.php";
    $req = Array();
    $req["email"] = $_POST["email"];
    $req["password"] = $_POST["password"];
    login($req);
    echo "<script>setTimeout('top.location = \'admin/index.php\'', 0);</script>";
} elseif (isset($_SESSION["username"])) {
    if ($_SESSION["username"] == "admin") {
        echo "<script>setTimeout('top.location = \'admin/index.php\'', 0);</script>";
    } else {
        $_SESSION['username'] = '';
        $_SESSION['id'] = 0;
        session_destroy();
        echo "<script>setTimeout('top.location = \'index.html\'', 0);</script>";
    }
} else {
    ?>
    <html>
    <head>
        <title>Работилничка</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <meta name="theme-color" content="#507742">
        <link rel="icon" sizes="124x124" href="favicon.ico">
        <script>
            function myFunction() {
                var x = document.getElementById("myTopnav");
                if (x.className === "topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "topnav";
                }
            }
        </script>

    </head>
    <body>
    <img class="logo" alt="logo" src="img/logo.png" usemap="#bgmap">
    <map name="bgmap">
        <area shape="circle" coords="136,53,25" href="videos.html"/>
    </map>
    <br/>
    <div class="topnav" id="myTopnav">
        <a href="index.html"> <img src="img/home.png" height="15" width="15"> </a>
        <a href="artic.php"> Артикули</a>
        <a href="about.html">За нас</a>
        <a href="more_info.html">Какво е онлайн магазин?</a>
        <a href="login.php" class="right"><img src="img/login.png"></a>
        <a href="javascript:void(0);" style="font-size:16.5px;" class="icon" onclick="myFunction()">&#9776;</a>
    </div>
    <br/>

    <div class="cen">
        <h2 align="center">Вход</h2>
        <form action="login.php" method="post">
            <div style="margin-left : 35%" class="query">
                <table>


                    <tr>
                        <td> Имейл:</td>
                        <td><input type="email" name="email"></td>
                    </tr>
                    <tr>
                        <td>
                            Парола:
                        </td>
                        <td><input type="password" name="password">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p align="center">
                                <button type="submit" name="submit" value="login" class="buttonOk">Вход</button>
                            </p>
                        </td>
                    </tr>

                </table>
            </div>
        </form>
        <p align="center" style="margin-left:5%">
            <button type="register" onclick="onRegister()" class="buttonImport">Регистрация</button>
        </p>
    </div>
    </body>
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }

        function onRegister() {
            window.location.href = "register.php";
        }
    </script>
    </html>
<?php } ?>