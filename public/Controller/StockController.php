<?php
/**
 * User: exname
 * Date: 2/9/19
 * Time: 2:58 PM
 */

require_once '/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Model/Stock.php';

function createStock($id,$price,$count)
{
    $stock = new Stock();
    $stock->createStock($id, $price, $count);
}

function updatePrice($id, $price)
{
    $stock = new Stock();
    $stock->updatePrice($id, $price);
}

function updateStock($id, $price,$count)
{
    $stock = new Stock();
    $stock->updateStock($id, $price, $count);
}

function decreaseCount($id,$count){
    $stock = new Stock();
    $stock->decCount($id, $count);
}
function increaseCount($id,$count){
    $stock = new Stock();
    $stock->incCount($id, $count);
}