<?php
/**
 * User: exname
 * Date: 2/9/19
 * Time: 2:58 PM
 */

require_once '/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Model/Items.php';

function create($request)
{
    $items = new Items();
    $items->createItem($request);
}

function delete($id)
{
    $items = new Items();
    $items->deleteItem($id);
}

function update($id, $request)
{
    $items = new Items();
    $items->updateItem($id, $request);
}

function readAll()
{
    $items = new Items();
    return $items->readItems();
}

function readOne($id)
{
    if ($id == null) {
        die();
    } else {
        $items = new Items();
        $item = $items->readItem($id);
        return $item;
    }
}