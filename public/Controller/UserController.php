<?php
/**
 * User: exname
 * Date: 2/9/19
 * Time: 2:58 PM
 */

require_once '/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Model/User.php';

function create($request)
{
    $user = new User();
    $user->createUser($request);
}

function delete($id)
{
    $user = new User();
    $user->deleteUser($id);
}

function update($id, $request)
{
    $user = new User();
    $user->updateUser($id, $request);
}

function readAll()
{
    $user = new User();
    return $user->readUsers();
}

function readOne($id)
{
    if ($id == null) {
        die();
    } else {
        $users = new User();
        $user = $users->readUser($id);
        return $user;
    }
}

function login($reqArray)
{
    $user = new User();
    $user->loginUser($reqArray);
}

function logout()
{
    session_destroy();
}