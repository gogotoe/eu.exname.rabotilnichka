<?php
/**
 * User: exname
 * Date: 2/9/19
 * Time: 2:58 PM
 */

require_once '/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Model/Order.php';

function deleteOrder($id)
{
    $orders = new Order();
    $orders->deleteOrder($id);
}

function readAllOrders()
{
    $orders = new Order();
    return $orders->readOrders();
}

function readOneOrder($id)
{
    if ($id == null) {
        die();
    } else {
        $orders = new Order();
        $order = $orders->readOrder($id);
        return $order;
    }
}

function createOrder($reqArray)
{
    $orders = new Order();
    $orders->createOrder($reqArray);
}