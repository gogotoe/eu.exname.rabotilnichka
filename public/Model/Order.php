<?php
/**
 * User: exname
 * Date: 2/9/19
 * Time: 12:39 PM
 */

require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/connection.php";
require_once "Picture.php";
require_once "Stock.php";
require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/StockController.php";
require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/Implementations/TextOut.php";

class Order
{

    private $out;

    public function __construct()
    {
        $out = new TextOut();
    }

    /*    public function updateOrder($id, $reqArray)
        {
            $dataConnection = new ConnectToDatabase();
            $connection = $dataConnection->connection;
            $resultArray = null;
            try {
                $sql = "UPDATE orders SET 'status_id'=?,'update_date'=current_timestamp WHERE id=?";
                $stmt = $connection->prepare($sql);
                $stmt->bind_param('ss', $reqArray["status_id"], $id);
                $stmt->execute();
            } catch (Exception $e) {

            } finally {
                mysqli_close($connection);
            }
            return $resultArray;
        }*/

    public function deleteOrder($id)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "SELECT item_id,count FROM orders WHERE id=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();
            $count = $row["count"];
            $itemId = $row["item_id"];
            $sql = "DELETE FROM orders WHERE id=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $stmt->execute();
            increaseCount($itemId, $count);
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

    }

    public function createOrder($reqArray)
    {

        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "INSERT INTO orders(item_id,count,user_id) VALUES (?,?,?)";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sss', $reqArray["itemId"], $reqArray["count"], $reqArray["userId"]);
            $stmt->execute();
            decreaseCount($reqArray["itemId"], $reqArray["count"]);
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

        return true;
    }

    public function readOrder($id)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $resultArray = null;
            $sql = "SELECT
                            i.id,
                            i.name,
                            s.price,
                            o.count,
                            CONCAT(u.first_name,
                            ' ',
                            u.last_name) AS names ,
                            u.address ,
                            u.email,
                            SUM(price*count) as total
                        FROM
                            items i,
                            stock s,
                            orders o,
                            users u
                        WHERE
                            o.id=?
                            AND o.user_id = u.id
                            AND i.id = s.itemId
                            AND i.id = o.item_id;";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $resultArray = array();
            $result = $stmt->get_result();

            while ($rowItem = $result->fetch_assoc()) {
                $resultArray["name"] = $rowItem["name"];
                $resultArray["size"] = $rowItem["size"];
                $resultArray["weight"] = $rowItem["weight"];
                $resultArray["price"] = $rowItem["price"];
                $resultArray["count"] = $rowItem["available_count"];
                $resultArray["picture"] = $rowItem["baseUrl"];
                $resultArray["thumbnail"] = $rowItem["thumbnailUrl"];
                $resultArray["date"] = $rowItem["date_put"];
                $resultArray["info"] = $rowItem["information"];
            }
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

        return $resultArray;
    }

    public function readOrders()
    {
        $allItems = array();
        $dataConnection = new ConnectToDatabase();
        try {
            $connection = $dataConnection->connection;
            $sql = "SELECT
                            o.id as oId,
                            i.id,
                            i.name,
                            s.price,
                            o.count,
                            CONCAT(u.first_name,
                            ' ',
                            u.last_name) AS names ,
                            u.address ,
                            u.email,
                            SUM(price*count) as total
                        FROM
                            items i,
                            stock s,
                            orders o,
                            users u
                        WHERE
                            o.user_id = u.id
                            AND i.id = s.itemId
                            AND i.id = o.item_id
                        GROUP BY
                            o.id";
            $stmt = $connection->prepare($sql);
            $stmt->execute();
            $result = $stmt->get_result();
            $resultArray = array();
            while ($rowItem = $result->fetch_assoc()) {
                $resultArray["oId"] = $rowItem["oId"];
                $resultArray["name"] = $rowItem["name"];
                $resultArray["id"] = $rowItem["id"];
                $resultArray["price"] = $rowItem["price"];
                $resultArray["total"] = $rowItem["total"];
                $resultArray["count"] = $rowItem["count"];
                $resultArray["userNames"] = $rowItem["names"];
                $resultArray["address"] = $rowItem["address"];
                $resultArray["email"] = $rowItem["email"];
                array_push($allItems, $resultArray);
            }

        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
        return $allItems;
    }

}