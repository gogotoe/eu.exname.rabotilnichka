<?php
/**
 * Created by PhpStorm.
 * User: exname
 * Date: 3/14/19
 * Time: 1:39 PM
 */

class CartPending
{
    public function __construct()
    {
    }

    public function updateCartItem($id, $reqArray)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        $resultArray = null;
        try {
            $sql = "UPDATE cart_pending SET 'count'=? WHERE user_id=? AND id=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sss', $reqArray["count"], $user_id, $id);
            $stmt->execute();
        } catch (Exception $e) {

        } finally {
            mysqli_close($connection);
        }
        return $resultArray;
    }

    public function deleteCartItem($id)
    {
        $dataConnection = new ConnectToDatabase();
        $bool = true;
        try {
            $connection = $dataConnection->connection;
            $sql = "DELETE FROM cart_pending WHERE user_id=? AND id=? ";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $stmt->execute();
        } catch (Exception $e) {
            //TODO::
            $bool = false;

        } finally {
            mysqli_close($connection);
            return $bool;
        }

    }

    public function createCartItem($reqArray)
    {

        $dataConnection = new ConnectToDatabase();
        try {
            $connection = $dataConnection->connection;
            $sql = "INSERT INTO orders(user_id,item_id,count) VALUES (?,?,?)";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sss', $reqArray["item_id"], $reqArray["item_id"], $reqArray["count"]);
            $stmt->execute();
            $itemId = $stmt->insert_id;
            $stock = new Stock();
            $stock->
            $stock->updateStock($reqArray["item_id"], $reqArray["count"]);

        } catch (Exception $e) {
            //TODO: add exeption to file
        } finally {
            mysqli_close($connection);
        }

        return true;
    }

    public function readCartItem($id)// TODO: Need time to thing
    {
        $dataConnection = new ConnectToDatabase();
        try {
            $connection = $dataConnection->connection;
            $resultArray = null;
            $sql = "SELECT name,weight,size,available_count,date_put,baseUrl,thumbnailUrl,price,available_count,information FROM items,picture,stock WHERE  items.id=? AND items.pictureId=picture.id";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $resultArray = array();
            $result = $stmt->get_result();

            while ($rowItem = $result->fetch_assoc()) {
                $resultArray["name"] = $rowItem["name"];
                $resultArray["size"] = $rowItem["size"];
                $resultArray["weight"] = $rowItem["weight"];
                $resultArray["price"] = $rowItem["price"];
                $resultArray["count"] = $rowItem["available_count"];
                $resultArray["picture"] = $rowItem["baseUrl"];
                $resultArray["thumbnail"] = $rowItem["thumbnailUrl"];
                $resultArray["date"] = $rowItem["date_put"];
                $resultArray["info"] = $rowItem["information"];
            }
        } catch (Exception $e) {
//TODO: add exeption to file
        } finally {
            mysqli_close($connection);
        }

        return $resultArray;
    }

    public function readCartItems()// TODO: more thing
    {
        $allItems = array();
        $dataConnection = new ConnectToDatabase();
        try {
            $connection = $dataConnection->connection;
            $sqlMain = "SELECT itemId FROM stock";
            $stmt = $connection->prepare($sqlMain);
            $stmt->execute();
            $resultMain = $stmt->get_result();
            foreach ($resultMain as $res) {
                $resultArray = array();
                $sql = "SELECT items.id,name,price,available_count,baseUrl,thumbnailUrl FROM items,stock,picture WHERE itemId=" . $res["itemId"] . " AND items.id=itemId AND items.pictureId=picture.id";
                $stmt = $connection->prepare($sql);
                $stmt->execute();
                $result = $stmt->get_result();

                while ($rowItem = $result->fetch_assoc()) {

                    $resultArray["name"] = $rowItem["name"];
                    $resultArray["id"] = $rowItem["id"];
                    $resultArray["price"] = $rowItem["price"];
                    $resultArray["count"] = $rowItem["available_count"];
                    $resultArray["picture"] = $rowItem["baseUrl"];
                    $resultArray["thumbnail"] = $rowItem["thumbnailUrl"];
                }
                array_push($allItems, $resultArray);
            }
        } catch (Exception $e) {
            //TODO::
        } finally {
            mysqli_close($connection);
        }
        return $allItems;
    }


}