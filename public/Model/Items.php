<?php
/**
 * User: exname
 * Date: 2/9/19
 * Time: 12:39 PM
 */

require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/connection.php";
require_once "Picture.php";
require_once "Stock.php";
require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/Implementations/TextOut.php";
require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/Controller/StockController.php";

class Items
{

    private $out;

    public function __construct()
    {
        $out = new TextOut();
    }

    public function updateItem($id, $reqArray)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "UPDATE items SET name=?,weight=?,size=?,date_update=current_timestamp,category=?,information=? WHERE id=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('ssssss', $reqArray["name"], $reqArray["weight"], $reqArray["size"], $reqArray["category"], $reqArray["info"], $id);
            $stmt->execute();
            updateStock($id, $reqArray["price"], $reqArray["count"]);
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
    }

    public function deleteItem($id)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "DELETE FROM items WHERE id=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $stmt->execute();
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

    }

    public function createItem($reqArray)
    {

        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $pictureID = uploadPicture();
            if ($pictureID != null) {
                $sql = "INSERT INTO items(name,weight,size,pictureId,information,category,date_put) VALUES (?,?,?,?,?,?,current_timestamp)";
                $stmt = $connection->prepare($sql);
                $stmt->bind_param('sssiss', $reqArray["name"], $reqArray["weight"], $reqArray["size"], $pictureID, $reqArray["info"], $reqArray["category"]);
                $stmt->execute();
                $itemId = $stmt->insert_id;
                createStock($itemId, $reqArray["price"], $reqArray["count"]);
            }
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

        return true;
    }

    public function readItem($id)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $resultArray = null;
            $sql = "SELECT name,weight,size,available_count,date_put,baseUrl,thumbnailUrl,price,available_count,information,category FROM items,picture,stock WHERE  items.id=? AND items.pictureId=picture.id";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $resultArray = array();
            $result = $stmt->get_result();

            while ($rowItem = $result->fetch_assoc()) {
                $resultArray["name"] = $rowItem["name"];
                $resultArray["size"] = $rowItem["size"];
                $resultArray["weight"] = $rowItem["weight"];
                $resultArray["price"] = $rowItem["price"];
                $resultArray["count"] = $rowItem["available_count"];
                $resultArray["picture"] = $rowItem["baseUrl"];
                $resultArray["thumbnail"] = $rowItem["thumbnailUrl"];
                $resultArray["date"] = $rowItem["date_put"];
                $resultArray["info"] = $rowItem["information"];
                $resultArray["category"] = $rowItem["category"];
            }
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

        return $resultArray;
    }

    public function readItems()
    {
        $allItems = array();
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sqlMain = "SELECT itemId FROM stock";
            $stmt = $connection->prepare($sqlMain);
            $stmt->execute();
            $resultMain = $stmt->get_result();
            foreach ($resultMain as $res) {
                $resultArray = array();
                $sql = "SELECT items.id,name,price,available_count,baseUrl,thumbnailUrl FROM items,stock,picture WHERE itemId=" . $res["itemId"] . " AND items.id=itemId AND items.pictureId=picture.id";
                $stmt = $connection->prepare($sql);
                $stmt->execute();
                $result = $stmt->get_result();

                while ($rowItem = $result->fetch_assoc()) {

                    $resultArray["name"] = $rowItem["name"];
                    $resultArray["id"] = $rowItem["id"];
                    $resultArray["price"] = $rowItem["price"];
                    $resultArray["count"] = $rowItem["available_count"];
                    $resultArray["picture"] = $rowItem["baseUrl"];
                    $resultArray["thumbnail"] = $rowItem["thumbnailUrl"];
                }
                array_push($allItems, $resultArray);
            }
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
        return $allItems;
    }

}