<?php
/**
 * User: exname
 * Date: 2/10/19
 * Time: 2:52 PM
 */

function Alert($msg)
{
    echo '<script type="text/javascript">alert("' . $msg . '")</script>';
}

function uploadPicture()
{
    $target_dir = "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/View/img/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {

            $uploadOk = 1;
        } else {
            Alert("File is not an image.");
            $uploadOk = 0;
            header("Refresh:0; url=index.php");
        }
    }

    if (file_exists($target_file)) {
        Alert("Sorry, file already exists.");
        $uploadOk = 0;
        header("Refresh:0; url=index.php");
    }

    if ($_FILES["fileToUpload"]["size"] > 5000000) {
        Alert("Sorry, your file is too large.");
        $uploadOk = 0;
        header("Refresh:0; url=index.php");
    }

    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        Alert("Sorry, only JPG, JPEG, PNG  files are allowed.");
        $uploadOk = 0;
        header("Refresh:0; url=index.php");
    }

    if ($uploadOk == 0) {
        Alert("Sorry, your file was not uploaded.");
        header("Refresh:5555; url=index.php");
    } else {

	$upload= move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
	if ($upload) {
            $thumb = makeThumbnails($target_file, date("ymdHis"), $imageFileType);
            require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/connection.php";
            $dataConnection = new ConnectToDatabase();
            $name = basename($_FILES["fileToUpload"]["name"]);
            $connection = $dataConnection->connection;
            $sql = "INSERT INTO picture(baseUrl,thumbnailUrl) VALUES (?,?)";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('ss', $name, $thumb);
            $stmt->execute();
            mysqli_close($connection);
            return $stmt->insert_id;
        } else {
            Alert("Sorry, there was an error uploading your file.");

            header("Refresh:656; url=index.php");
        }
    }
}


function makeThumbnails($IMG, $name, $type)
{
	 Alert("OK");

    $thumbnail_width = 134;
    $thumbnail_height = 189;
    $thumb_beforeWord = "thumb";
    $arr_image_details = getimagesize($IMG);
    $original_width = $arr_image_details[0];
    $original_height = $arr_image_details[1];
    if ($original_width > $original_height) {
        $new_width = $thumbnail_width;
        $new_height = intval($original_height * $new_width / $original_width);
    } else {
        $new_height = $thumbnail_height;
        $new_width = intval($original_width * $new_height / $original_height);
    }
    $dest_x = intval(($thumbnail_width - $new_width) / 2);
    $dest_y = intval(($thumbnail_height - $new_height) / 2);
    if ($arr_image_details[2] == IMAGETYPE_GIF) {
        $imgt = "ImageGIF";
        $imgcreatefrom = "ImageCreateFromGIF";
    }
    if ($arr_image_details[2] == IMAGETYPE_JPEG) {
        $imgt = "ImageJPEG";
        $imgcreatefrom = "ImageCreateFromJPEG";
    }
    if ($arr_image_details[2] == IMAGETYPE_PNG) {
        $imgt = "ImagePNG";
        $imgcreatefrom = "ImageCreateFromPNG";
    }
    if ($imgt) {
        $old_image = $imgcreatefrom($IMG);
        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
        imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
        $imgt($new_image, "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/public/View/img/" . "$name" . '_' . "$thumb_beforeWord" . ".$type");
    }
    return "$name" . '_' . "$thumb_beforeWord" . ".$type";
}
