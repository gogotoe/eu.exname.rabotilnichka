<?php
/**
 * User: exname
 * Date: 2/10/19
 * Time: 6:35 PM
 */

require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/connection.php";
require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/Implementations/TextOut.php";

class Stock
{
    private $out;

    public function __construct()
    {
        $out = new TextOut();
    }

    public function createStock($itemId, $price, $count)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "INSERT INTO stock(itemId,price,available_count) VALUES ( ?,?,?)";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sds', $itemId, $price, $count);
            $stmt->execute();
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

    }

    public function updateCount($itemId, $count)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "UPDATE stock SET count= (?) WHERE itemId=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('ss', $count, $itemId);
            $stmt->execute();
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
    }

    public function updateStock($itemId, $price, $count)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "UPDATE stock SET available_count= ? , price=? WHERE itemId=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sds', $count, $price, $itemId);
            $stmt->execute();
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
    }

    public function decCount($itemId, $count)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "UPDATE stock SET available_count= (SELECT available_count FROM (SELECT * from stock) s WHERE itemId=?)-? WHERE itemId=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sss', $itemId, $count, $itemId);
            $stmt->execute();
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
    }

    public function incCount($itemId, $count)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "UPDATE stock SET available_count= (SELECT available_count FROM (SELECT * from stock) s WHERE itemId=?)+? WHERE itemId=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sss', $itemId, $count, $itemId);
            $stmt->execute();
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
    }

    public function updatePrice($itemId, $price)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "UPDATE stock SET price=? WHERE itemId=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('ss', $price, $itemId);
            $stmt->execute();
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
    }

    public function getCountStock($itemId)
    {
        $count = 0;
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $sql = "SELECT available_count FROM stock WHERE itemId=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $itemId);
            $stmt->execute();
            $result = $stmt->get_result();

            while ($rowItem = $result->fetch_assoc()) {
                $count = $rowItem["count"];
            }
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }
        return $count;

    }
}
