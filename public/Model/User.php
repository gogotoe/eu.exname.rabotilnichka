<?php
/**
 * Created by PhpStorm.
 * User: exname
 * Date: 3/14/19
 * Time: 3:23 PM
 */

require_once "Mail.php";
require_once "/var/www/html/rabotilnichka_beta/eu.exname.rabotilnichka/connection.php";

class User
{
    public function readUser($id)
    {
        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $resultArray = null;
            $sql = "SELECT username,first_name,last_name,email FROM users WHERE  id=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $resultArray = array();
            $result = $stmt->get_result();

            while ($rowItem = $result->fetch_assoc()) {
                $resultArray["username"] = $rowItem["username"];
                $resultArray["first_name"] = $rowItem["first_name"];
                $resultArray["last_name"] = $rowItem["last_name"];
                $resultArray["email"] = $rowItem["email"];
            }
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

        return $resultArray;
    }

    public function createUser($reqArray)
    {

        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $hash = md5(rand());
            $pass = password_hash($reqArray["password"], PASSWORD_BCRYPT);
            $sql = "INSERT INTO users(username,first_name,last_name,password,address,email,hash) VALUES (?,?,?,?,?,?,?)";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('sssssss', $reqArray["username"], $reqArray["firstName"], $reqArray["lastName"], $pass, $reqArray["address"], $reqArray["email"], $hash);
            $stmt->execute();
            /*            $email = new Mail();
                        $email->sendRegisterEmail($reqArray["email"], $hash);*/
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

        return true;
    }

    public function loginUser($reqArray)
    {

        $dataConnection = new ConnectToDatabase();
        $connection = $dataConnection->connection;
        try {
            $pass = $reqArray["password"];
            $sql = "SELECT id,username ,password FROM users WHERE email=?";
            $stmt = $connection->prepare($sql);
            $stmt->bind_param('s', $reqArray["email"]);
            $stmt->execute();
            $result = $stmt->get_result();
            $password = "";
            $id = 0;
            while ($rowItem = $result->fetch_assoc()) {
                $password = $rowItem["password"];
                $username = $rowItem['username'];
                $id = $rowItem['id'];
            }
            $password = substr($password, 0, 60);
            if (isset($username) && password_verify($pass, $password)) {
                $_SESSION['username'] = $username;
                $_SESSION['id'] = $id;
            }
        } catch (Exception $e) {
            $this->out->print($e . "/n");
        } finally {
            mysqli_close($connection);
        }

        return true;
    }
}
