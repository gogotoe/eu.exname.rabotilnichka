<?php


class Mail
{
    private $url = "http://rabotilnichka.exname.eu";

    private function sendEmail($msg, $to, $title)
    {
        $headers = array(
            'From' => 'no.reply@exname.eu',
            'Reply-To' => 'no.reply@exname.eu',
            'X-Mailer' => 'PHP/' . phpversion()
        );

        mail($to, $title, $msg, $headers);
    }

    public function sendRegisterEmail($to, $hash)
    {
        $msg = "Здравейте, \n  с Вашия email адрес бе направена регистрация, ако желаете да я потвърдите моля посетете : <a href='" . $this->url . "/register.php?code='" . $hash . ">";
        $title = "Потвърдете регистрацията си";
        $this->sendEmail($msg, $to, $title);
    }

}