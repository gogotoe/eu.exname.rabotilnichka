<?php
/**
 * Created by PhpStorm.
 * User: exname
 * Date: 3/14/19
 * Time: 3:42 PM
 */

namespace OutputInterface;

interface OutputInterface
{
    public function print($msg);

}