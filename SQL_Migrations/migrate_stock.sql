CREATE TABLE stock
(
    id     INT(6) AUTO_iNCREMENT PRIMARY KEY,
    itemId INT(6)       NOT NULL,
    available_count VARCHAR(4),
    price  DOUBLE(8, 2) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;