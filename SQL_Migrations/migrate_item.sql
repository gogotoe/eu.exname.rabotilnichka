CREATE TABLE items
(
    id              INT(6) AUTO_iNCREMENT NOT NULL PRIMARY KEY,
    name            VARCHAR(36)           NOT NULL,
    weight          VARCHAR(18)           NOT NULL,
    size            VARCHAR(64)           NOT NULL,
    pictureId       VARCHAR(6),
    information     VARCHAR(1024)         NOT NULL,
    category        VARCHAR(64)           NOT NULL,
    date_put        TIMESTAMP                      DEFAULT CURRENT_TIMESTAMP,
    date_update     TIMESTAMP                      DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

