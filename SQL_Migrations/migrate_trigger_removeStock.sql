DELIMITER $$
CREATE TRIGGER removeStock
    AFTER DELETE
    ON rabotilnichka.items
    FOR EACH ROW
BEGIN
    DELETE
    FROM rabotilnichka.stock
    WHERE itemId = OLD.id;
END$$

DELIMITER ;