CREATE TABLE users
(
    id         INT(6)       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username   VARCHAR(36)  NOT NULL UNIQUE,
    first_name VARCHAR(36)  NOT NULL,
    last_name  VARCHAR(36)  NOT NULL,
    password   VARCHAR(128) NOT NULL,
    address    VARCHAR(256) NOT NULL,
    email      VARCHAR(36)  NOT NULL UNIQUE,
    isActive   BOOLEAN DEFAULT TRUE,
    hash       VARCHAR(64)  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;