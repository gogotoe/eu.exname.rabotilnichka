CREATE TABLE picture
(
  id           INT(6) AUTO_iNCREMENT NOT NULL PRIMARY KEY,
  baseUrl      VARCHAR(255)           NOT NULL,
  thumbnailUrl VARCHAR(255)           NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;