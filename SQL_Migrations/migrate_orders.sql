CREATE TABLE orders
(
    id          INT(6) AUTO_iNCREMENT NOT NULL PRIMARY KEY,
    item_id     INT(6)                NOT NULL,
    user_id     INT(6)                NOT NULL,
    count       INT(2)                NOT NULL,
    order_date  TIMESTAMP                      DEFAULT CURRENT_TIMESTAMP,
    update_date TIMESTAMP                      DEFAULT CURRENT_TIMESTAMP,
    status_id   INT(1)                NOT NULL DEFAULT 0

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;