CREATE TABLE cart_pending
(
  id      INT(2) AUTO_INCREMENT NOT NULL PRIMARY KEY,
  user_id INT(6)                NOT NULL,
  item_id INT(6)                NOT NULL,
  count   INT(2)                NOT NULL

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;