CREATE TABLE status (
  id INT(1) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  name VARCHAR(36) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;